package com.example.clicker;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.example.clicker.models.Score;
import com.example.clicker.repositories.ScoreRepository;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener { // View.OnClickListener,
    private TextView textTime;
    private TextView textTap;
    private TextView topScore;
    private TextView highScoresBtn;

    private boolean state = false;
    private int count = 10;
    private int clickCount;
    private int clickReward;

    private Score maxScore;
    private Timer mTimer;
    private ScoreRepository scoreRepository;

    private ImageView imageView;
    private TextView bonus;
    private ViewGroup root;

    GameSound gameSound;

    @SuppressLint({"SetTextI18n", "ClickableViewAccessibility"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        hideNavBar();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // подключаем БД и CRUD-функции
        scoreRepository = ScoreRepository.getInstance(this);

        gameSound = new GameSound(this);

        // инициализируем view-объекты
        root = findViewById(R.id.container);
        textTime = findViewById(R.id.textTime);
        textTap = findViewById(R.id.textTap);
        topScore = findViewById(R.id.topScore);
        highScoresBtn = findViewById(R.id.highScores);
        bonus = findViewById(R.id.bonusTime);

                imageView = new ImageView(this);
        imageView.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        root.addView(imageView);

        // присваиваем обработчик кликов
        highScoresBtn.setOnClickListener(clickListener);
        root.setOnTouchListener(this);

        // устанавливаем значение "Top score"
        maxScore = scoreRepository.getMaxScore();
        if (maxScore == null) maxScore = new Score();
        topScore.setText("Top score: " + maxScore.getScore());
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, HighScore.class);
            startActivity(intent);
        }
    };

    @SuppressLint("SetTextI18n")
    private void funcClick() {
        hideNavBar();
        if (!state) {
            highScoresBtn.setVisibility(View.INVISIBLE);
            count = 10;
            clickCount = 0;
            clickReward = 0;
            textTap.setText(String.valueOf(0));
            if (mTimer != null) {
                mTimer.cancel();
                mTimer = null;
            }

            mTimer = new Timer();
            MyTimerTask mMyTimerTask = new MyTimerTask();

            state = true;
            mTimer.schedule(mMyTimerTask, 0, 1000);

            clickCount++;
            clickReward++;
            textTap.setText(String.valueOf(clickCount));
        } else {
            clickCount++;
            clickReward++;
            textTap.setText(String.valueOf(clickCount));
        }
        if (clickReward == 50) {
            bonus.setVisibility(View.VISIBLE);
            bonus.startAnimation(AnimationUtils.loadAnimation(this, R.anim.bonus_time));
            bonus.setVisibility(View.INVISIBLE);
            count += 5;
            clickReward = 0;
            textTime.setText("Time: " + count);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            gameSound.onPlay();

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(100, 100);
            layoutParams.leftMargin = (int) event.getX() - 50;
            layoutParams.topMargin = (int) event.getY() - 50;
            imageView.setLayoutParams(layoutParams);

            imageView.setImageResource(R.drawable.source_it);

            imageView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.click));
            imageView.setVisibility(View.INVISIBLE);
            funcClick();
        }
        return true;
    }

    class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            count--;

            runOnUiThread(new Runnable() {
                @SuppressLint("SetTextI18n")
                @Override
                public void run() {
                    if (count >= 0) {
                        textTime.setText("Time: " + count);
                    } else if (count == -1) {
                        textTap.setText("Result: " + clickCount);
                        root.setEnabled(false);

                        if (clickCount > maxScore.getScore()) { // обвновление топа
                            topScore.setText("Top score: " + clickCount);
                            maxScore.setScore(clickCount);
                        }

                        if (scoreRepository.getCount() < 10) { // сохранение результата
                            scoreRepository.create(clickCount);
                        } else {
                            Score minScore = scoreRepository.getMinScore();
                            if (clickCount > minScore.getScore()) {
                                scoreRepository.deleteScore(minScore.getId());
                                scoreRepository.create(clickCount);
                            }
                        }

                        highScoresBtn.setVisibility(View.VISIBLE);
                    } else if (count == -3) {
                        root.setEnabled(true);
                        mTimer.cancel();
                        state = false;
                    }
                }
            });
        }
    }

    private void hideNavBar() {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_IMMERSIVE
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    @Override
    protected void onStart() {
        hideNavBar();
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        scoreRepository.close();
    }
}