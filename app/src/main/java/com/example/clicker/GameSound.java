package com.example.clicker;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class GameSound implements SoundPool.OnLoadCompleteListener {
    private SoundPool sp;
    private int soundIdShot;

    GameSound(Context context) {
        sp = new SoundPool(2, AudioManager.STREAM_MUSIC, 0);
        sp.setOnLoadCompleteListener(this);
        soundIdShot = sp.load(context, R.raw.tap, 1);
    }

    void onPlay() {
        sp.play(soundIdShot, 1, 1, 0, 0, 1);
    }

    @Override
    public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
    }
}
