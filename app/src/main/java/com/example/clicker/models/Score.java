package com.example.clicker.models;

import android.text.format.DateFormat;

import java.util.Date;
import java.util.Objects;

import lombok.Data;

@Data
public class Score {
    private long id;
    private int score;
    private String date;

    public Score() {
        this.date = DateFormat
                .format("dd-MM-yyyy HH:mm:ss", new Date())
                .toString();
    }

    public Score(int score) {
        this.score = score;
        this.date = DateFormat
                .format("dd-MM-yyyy HH:mm:ss", new Date())
                .toString();
    }

    public Score(long id, int score, String date) {
        this.id = id;
        this.score = score;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Score score1 = (Score) o;
        return id == score1.id &&
                score == score1.score &&
                Objects.equals(date, score1.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, score, date);
    }
}
