package com.example.clicker.configuration;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
    private static DBHelper instance;
    private static final String DB_NAME = "clickerDB";

    private DBHelper(Context context) {
        // конструктор суперкласса
        super(context, DB_NAME, null, 1);
    }

    public static DBHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DBHelper(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("DBHelper.log", "Create database: "+DB_NAME);
        // создаем таблицу с полями
        db.execSQL(
                "create table high_score (" +
                        "id integer primary key autoincrement," +
                        "score integer," +
                        "date text);"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
