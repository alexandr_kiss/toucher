package com.example.clicker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.clicker.models.Score;
import com.example.clicker.repositories.ScoreRepository;

import java.util.ArrayList;
import java.util.List;

public class HighScore extends AppCompatActivity {
    private List<Score> scores;
    private ListView lvMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.high_score);

        // подключаем БД и CRUD-функции
        ScoreRepository scoreRepository = ScoreRepository.getInstance(this);

        scores = scoreRepository.getAllScore();

        // находим список
        lvMain = findViewById(R.id.lvMain);

        // создаем адаптер
        ArrayAdapter<Score> adapter = new ScoreAdapter(this);

        // присваиваем адаптер списку
        lvMain.setAdapter(adapter);
    }

    private class ScoreAdapter extends ArrayAdapter<Score> {
        public ScoreAdapter(Context context) {
            super(context, R.layout.item, scores);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Score score = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext())
                        .inflate(R.layout.item, lvMain, false);
            }

            ((TextView) convertView.findViewById(R.id.text))
                    .setText(score.getScore()+"");
            ((TextView) convertView.findViewById(R.id.text2))
                    .setText(score.getDate());
            return convertView;
        }
    }
}